![alt text](http://i.imgur.com/WFBqJ2D.png "Hermes Chat Logo")
#### Hermes Chat Source - v5.0.0
##### Hermes Chat Online - v2.2.4

## Hermes Chat | AlterCodeStudios
### Introduction
Hermes Chat is a fully open-source chat. It first started developement in late 2013 and has only gotten better. Recently a decision was made to rebuild the chat using the CodeIgniter framework so we are a bit behind when it comes to features, but code wise...it's much better. Only major thing we ask is that you do not take our work and claim it as your own.

One quick thing, if you would like to suggest an idea for Hermes Chat for us to add you can submit it [here](http://www.ideas.altercodestudios.com)
Make sure you use the Hermes Chat category when submitting your idea!
----
### Installation
##### Installation section will be redone with time.

#### Roles/Ranks

`0` = Banned

`1` = Administrator

`2` = Staff

`3` = Moderator

`4` = Member

----
### To-Do List
##### Almost Complete
>> New framework, will take some time to complete anything.

##### High Priority
+ Main Chat
+ Profiles

##### Normal Priority
+ User Control Panel | Won't be normal priority forever, just until everything in high priority is set up.
+ Administrator Control Panel
+ Announcemnets

##### Low Priority
+ In chat smilies
+ In chat URLs
+ Private Messaging
+ Reporting Users
+ Userbars


##### Completed
+ New framework
+ Log in
+ Registration

----
### Contact
If you would like to contact us for any sort of support, email support@altercodestudios.com

For anything else, please email hermes@altercodestudios.com

----
### Links
[AlterCodeStudios](http://www.altercodestudios.com)

[Matthew](http://www.youtube.com/ColdfireTube)
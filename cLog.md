!!-- Hermes Chat Changelog <Public> --!!
Beta v5.0.0 (Ethera)
- New...EVERYTHING! No longer using the out of date core, we're now working of CodeIgniter! 
- New homepage; no longer takes you to a chat page, but it will take you to a log in/landing page. This will improve speed and overall look of the site.
- New database table to store all the new info in! 
** Developer's Comments ** 
>> Hey! Matthew here! I'm really excited to provide this new update to everyone! It's a huge thing with only a little bit done.
>> I know the only thing done right is the log in and the create account functions, but I'm working my hardest to try to bring everything back up to speed. Give it some time!

Beta v2.2.4 (Diana)
- User Defaults are now added, edit the info in 'classes/CUserDefaults.php'.
- External registration page added.
- Index JavaScript bugs fixed.
- Sending/Recieving messages in main chat sped up, however slight glitches occur but do not affect user or messages.
- Optimized connection between database and site.
- Site optimized for multiple devices, however control panel widget bug with uploading still occurs.
- Slight UI image/button glitches have be resolved.
- Log in form has been altered for looks.
- Multi-page navigation is complete (no other pages have been added to top navigation bar yet).
- Banned user role/level now added.
- IP address is now logged correctly.
- Bug with incorrect encryption of passwords is fixed.
- Vulnerability where hacker could access MySQL file has been resolved.
- User profile customization has been removed until further notice.
- Spam protection has been added on the chat.
- User registration vulnerability has been patched.
- The sending of HTML over chat has been fixed.
- Announcements added.
- Profile userbars added.
- User control panel added.

Beta v2.1.1 (Diana)
- Add Bootstrap interface
- Responsive layout added
-> Multi-Page navigation is near complete, soon to be released
-> Admin functions on the way for chat

Beta v1i.1.1 (Carrie)
- Removed Bootstrap interface
- Slightly different user interface <Looks more like the original now>
-> Multi-Page navigaiton and other functions are being worked on
-> Admin functions on the way for global chat
-> Multi chat room support is far from testing, but being worked on.

Beta v1.1.1 (Carrie)
- New user interface
- Added a home page
- Moved chat to it's own page
- Temporarily removed sidebar along with private chat
- Added footer <with version number & copyright>

Beta v0.1.1 (Betsy)
- Added donation link.
- Removed ads that I forgot to take out in v0.0.1
- Added logo
- Added version text to top header
- Fixed private messaging glitch <Private messaging still not where near complete though>
- Fixed the glitch where the text wouldn't clear after sending
- Changed some registration stuff

Beta v0.0.1 (Angel)
- Public release <No major changes>

List of Beta Names:
Angel -> v0.0.1
Betsy -> v0.1.1
Carrie -> v1.1.1/v1i.1.1
Diana -> v2.1.1/v2.2.4
Ethera -> v5.0.0

X.Y.Z
X: Core changes
Y: Minor changes
Z: Design Changes
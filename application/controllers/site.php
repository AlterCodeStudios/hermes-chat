<?php

class Site extends CI_Controller
{
	function __contstruct()
	{
		parent::Controller();
		$this->is_logged_in();
	}
	
	function is_logged_in()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		
		if(!isset($is_logged_in) || $is_logged_in != true)
		{
			echo 'Not logged in';
			die();
		}
	}
	
	function chat()
	{
		if('is_logged_in' == true)
		{	
				$data['main_content'] = 'chat';
				$this->load->view('includes/template', $data);
		}
		
		else
		{
			
		}
	}
		
	function logout()
	{
    	$this->session->sess_destroy();
		redirect('');
	}
}
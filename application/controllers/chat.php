<?php

class Chat extends CI_Controller
{
	public function index()
	{
		$getMessage = $this->input->post('message', TRUE);
		echo $getMessage;
	}
	
	function __contstruct()
	{
		parent::Controller();
		$this->is_logged_in();
	}
	
	function home()
	{
		$this->load->view('chat');
	}
	
	function chat()
	{
		$this->load->model('membership_model');
		$query = $this->membership_model->validate();
		
		if($query)
		{
			$data = array(
				'username' => $this->input->post('username'),
				'is_logged_in' => true
				);
				
				$this->session->set_userdata($data);
				$data['main_content'] = 'chat';
				$this->load->view('includes/template', $data);
		}
		
		else
		{
			$this->index();
		}
		
	}
	
	function is_logged_in()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		
		if(!isset($is_logged_in) || $is_logged_in != true)
		{
			echo 'Not logged in';
			die();
		}
	}
	
	function postMessage()
	{
		/*$this->load->model('messageController');
		$this->messageController->addMessage();*/
		/*$sender = $this->db->select(username);
		$query = $this->db->get('members');
		$bannedWords = array('testword', 'cuss');
		$postMessage = $this->input->post['message'];
		$postMessage = word_censor($postMessage, $bannedWords, '****');*/
		$insertChatMessage = array(
			'Sender' => '56563',
			'Message' => 'Message',
			'Time' => '123',
			'Room' => '0',
			'Type' => '0'
		);
		$insert = $this->db->insert('chat_messages', $insertChatMessage);
		return $insert;
	}
	
	function logout()
	{
    	$this->session->sess_destroy();
		redirect('');
	}
}
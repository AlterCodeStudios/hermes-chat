<div id="login_form">
	<center><img src="http://hermes.altercodestudios.com/images/logo.png" width="300px" height="70px" title="Hermes Chat" /></center>
    </br>
    <?php
	$username_input = array(
              'name' => 'username',
              'id' => 'username',
			  'placeholder' => 'Username'
            );
	$password_input = array(
              'name' => 'password',
              'id' => 'password',
			  'placeholder' => 'Password'
            );

		
	echo form_open('login/validate_credentials');
	echo form_input($username_input);
	echo form_password($password_input);
	echo form_submit('submit', 'Login');
	echo anchor('login/signup', 'Create Account');
	?>
</div>
<h1>Create An Account</h1>

<fieldset>
	<legend>Personal Information</legend>
    
    <?php
		$fname_input = array(
              'name' => 'first_name',
              'value' => set_value('first_name', ''),
			  'placeholder' => 'First Name'
            );
		$lname_input = array(
              'name' => 'last_name',
              'value' => set_value('last_name', ''),
			  'placeholder' => 'Last Name'
            );
		$eaddress_input = array(
              'name' => 'email_address',
              'value' => set_value('email_address', ''),
			  'placeholder' => 'Email Address'
            );
		echo form_open('login/create_member');
		echo form_input($fname_input);
		echo form_input($lname_input);
		echo form_input($eaddress_input);
	?>
</fieldset>

<fieldset>
	<legend>Hermes Chat Information</legend>
    
    <?php
	$username_input = array(
              'name' => 'username',
              'value' => set_value('username', ''),
			  'placeholder' => 'Username'
            );
	$password_input = array(
              'name' => 'password',
			  'value' => set_value('password', ''),
			  'placeholder' => 'Password'
            );
	$password_input_confirm = array(
              'name' => 'password2',
              'value' => set_value('password2', ''),
			  'placeholder' => 'Confirm Password'
            );
			
		echo form_input($username_input);
		echo form_password($password_input);
		echo form_password($password_input_confirm);
		
		echo form_submit('submit', 'Create Account');
	?>
    
    <?php echo validation_errors('<p class="error">'); ?>
</fieldset>